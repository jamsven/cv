class Contact < MailForm::Base
  attribute :name
  attribute :email
  attribute :message 

  validates :name, presence: true, length: { maximum: 100 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 200 }, format: { with: VALID_EMAIL_REGEX }
  validates :message ,  presence: true, length: { maximum: 600 }

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Cv Contact Form",
      :to => "d.knafelj@gmail.com",
      :from => %("#{name}" <#{email}>)
    }
  end
end