class StaticPagesController < ApplicationController
  def home
    require 'geoip'
    @geoip =  GeoIP.new(Rails.root.join("GeoLiteCity.dat")).city(request.remote_ip)
    @contact = Contact.new()
  end
end
