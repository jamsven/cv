// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require turbolinks
//= require jQuery-One-Page-Nav
//= require_tree .

$(function(){ 
      $(document).foundation();
      $('#navigation').onePageNav({
        currentClass: 'active',
        changeHash: false,
        scrollSpeed: 750,
        scrollThreshold: 0.5,
        filter: '',
        easing: 'swing',
        begin: function() {
            //I get fired when the animation is starting
        },
        end: function() {
            //I get fired when the animation is ending
        },
        scrollChange: function($currentListItem) {
            //I get fired when you enter a section and I pass the list item of the section
        }
    });

    $('#new_contact').bind('ajax:before',function(){
     console.log("beforeSend");
     $('.loader').show();
     $('input[type="submit"]').addClass('disabled');

    }).bind('ajax:complete', function(){
        console.log("complete");
        $('.loader').hide();
        $('input[type="submit"]').removeClass('disabled');
        $(document).foundation('alert', 'reflow');
    });

 });

$.ajaxSetup({
  dataType: 'json'
})
