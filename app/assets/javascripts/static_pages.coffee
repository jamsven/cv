# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


#$.ajax(url: "/contacts/new").done (html) ->
#  $("#contact").append html

#$(document).ready ->
#  $("#contact").on("ajax:success", (e, data, status, xhr) ->
#    $("#contact").replaceWith xhr.responseText
#  ).on "ajax:error", (e, xhr, status, error) ->
#    $("#contact").replaceWith "<p>ERROR</p>"


$(document).ready ->
  $.fn.render_form_errors = (model_name, errors) ->
    this.clear_form_errors()

    $.each(errors, (field, messages) ->
      field_name =  "*[name~='" + model_name + "[" + field + "]']"
      field = $(field_name)
      $('<small class="error">' + messages + '</small>' ).insertAfter field
      $(field).parent().addClass("error")
    )

  $("#new_contact").on("ajax:success", (e, data, status, xhr) ->
    $("#contact form").clear_form_fields()
    $("#contact form").prepend '<div data-alert class="alert-box success radius">Dear ' + data['name'] + ', thank you for your message. I\'ll respond as soon as possible!<a href="#" class="close">&times;</a>
</div>'
    
  ).on("ajax:error", (e, data, status, xhr) ->
    $("#contact form").render_form_errors('contact', data.responseJSON)
  )

  $.fn.clear_form_errors = () ->
    this.find('small.error').remove()
    this.find('.error').removeClass('error')

  $.fn.clear_form_fields = () ->
    this.clear_form_errors()
    this.find(':input','#new_contact')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected')
        